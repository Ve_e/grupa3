using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class Test : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.W))
        {
            transform.Translate(Vector3.forward*0.2f);
        }
        if (Input.GetKey(KeyCode.D))
        {
            transform.Translate(Vector3.right*0.2f);
        }
        if (Input.GetKey(KeyCode.A))
        {
            transform.Translate(Vector3.left*0.2f);
        }
        if (Input.GetKey(KeyCode.S))
        {
            transform.Translate(Vector3.back*0.2f);
        }
        if (Input.GetKey(KeyCode.E))
        {
            transform.Translate(Vector3.up*0.1f);
        }
        if (Input.GetKey(KeyCode.Q))
        { 
            transform.Translate(Vector3.down*0.1f);
        }
        

    }
}
